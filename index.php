<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>jQuery.getJSON demo</title>
  <style>
  img {
    height: 100px;
    float: left;
  }
  </style>
  <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
</head>
<body>
<textarea id="jdata" rows="20" cols="50"></textarea>
<button onClick="GenerateReport()">Generate Report</button> 
<div id="id01"></div>
<script>
function myFunction(arr) {
    var out = "<ul>";
    var i;
	var s;
    for(i = 0; i<arr.length; i++) {
		
		if(arr[i].type=="composition")
		{
			out+="<ul>";
			for(j = 0; j<arr[i].composition.length; j++)
			{
				if(arr[i].composition[j].type=="composition")
				{
					out+="<ul>";
					for(k = 0; k<arr[i].composition[j].composition.length; k++)
					{
						s=(arr[i].composition[j].composition[k].format) ? arr[i].composition[j].composition[k].format : "Text";
						out += '<li>' + arr[i].composition[j].composition[k].name + ':'+ s +'</li>';
					}
					out+="</ul>";
				}
				else
				{
					s=(arr[i].composition[j].format) ? arr[i].composition[j].format : "Text";
					out += '<li>' + arr[i].composition[j].name + ':'+ s +'</li>';
				}
			}
			out+="</ul>";
			
		}
		else
		{
			s=(arr[i].format) ? arr[i].format : "Text";
			out += '<li>' + arr[i].name + ':'+ s +'</li>';
		}
	
	
        
    }
    document.getElementById("id01").innerHTML = out+"</ul>";
}
function GenerateReport()
{
	if(document.getElementById("jdata").value=="")
	{
		myFunction(defaultComposition);
	}
	else
	{
		var d=document.getElementById("jdata").value;
		myFunction(d);
	}
}
</script>
<script src="my.js"></script>
</body>
</html>